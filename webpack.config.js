var webpack = require("webpack");
module.exports = {
    entry: {
        "main": [
            "webpack-dev-server/client?http://localhost:8882/",
            "webpack/hot/only-dev-server",
            "./main/main.jsx"
        ] },
    output: {
        path: '/',
        filename: "main.js",
        publicPath: "http://localhost:8882/",
        chunkFilename: "[id].chunk.js",
        sourceMapFilename: "main.js.map"
    },
    resolve: {
        extensions: ["*",".js", ".jsx", ".es6",".tsx", ".json"],
        modules: ["node_modules"]
    },
    module: {
        loaders: [
            {
                test: /\.jsx$|\.es6$|\.js$/,
                loader: "babel-loader",
                query:
                    {
                        presets:["es2015", "react"]
                    },
                exclude: /node_module/},
            { test: /\.scss$|\.css$/, loader: "style-loader!style!css!sass" }
        ]
    },
    plugins:
        [
            new webpack.NoEmitOnErrorsPlugin()
        ],
    devtool: "eval-source-map"
};