import React from 'react'
import ReactDOM from 'react-dom'

const CONTACTS = [
    {
        id: 1,
        name: 'Darth Vader',
        phoneNumber: '+250966666666',
        image: '/main/img/darth.gif'
    }, {
        id: 2,
        name: 'Princess Leia',
        phoneNumber: '+250966344466',
        image: '/main/img/leia.gif'
    }, {
        id: 3,
        name: 'Luke Skywalker',
        phoneNumber: '+250976654433',
        image: '/main/img/luke.gif'
    }, {
        id: 4,
        name: 'Chewbacca',
        phoneNumber: '+250456784935',
        image: '/main/img/chewbacca.gif'
    }
];

class Contact extends React.Component {
    constructor(props){
        super(props);
    }
    render() {
        return <li className="contact">
            <img className="contact-image" src={this.props.image} width="60px" height="60px" />
            <div className="contact-info">
                <div className="contact-name">{this.props.name}</div>
                <div className="contact-number">{this.props.phoneNumber}</div>
            </div>
        </li>
    }
}

class Timer extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            seconds: 0
        };

        this.tick = this.tick.bind(this);
    }
    componentDidMount() {
        this.timer = setInterval(this.tick, 1000);
    }

    tick () {
        this.setState({seconds: this.state.seconds + 1});
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    render() {
        return (
            <h4>Here for {this.state.seconds}</h4>
        );
    }


}
class ContactList extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            displayedContacts: CONTACTS
        };
        this.handleSearch = this.handleSearch.bind(this);

    }

    handleSearch(event) {
       let searchQuery = event.target.value.toLowerCase();
       let displayedContacts = CONTACTS.filter(function(el){
           let searchValue = el.name.toLowerCase();
           return searchValue.indexOf(searchQuery) !== -1;
       });
       this.setState({
           displayedContacts : displayedContacts
       });

    }

    componentWillReceiveProps(props) {
        console.log('componentWillReceiveProps');
        console.log(props);
    }


    shouldComponentUpdate() {
        console.log('shouldComponentUpdate()');
        return true;
    }

    componentWillMount() {

    }

    componentDidMount() {
        console.log('componentDidMount');
    }

    render()  {
        console.log(CONTACTS);
        return (
            <div className="contacts">
                <input type="text" className="search-field" onChange={this.handleSearch}/>
                <ul className="contacts-list">
                    {
                        this.state.displayedContacts.map(function(el) {
                            return <Contact key={el.id} phoneNumber={el.phoneNumber} image={el.image} name={el.name}/>
                        })
                    }
                </ul>
            </div>
        );
    }
}

ReactDOM.render(
    <div>
        <Timer/>
        <ContactList/>
    </div>,
    document.getElementById('content')
);